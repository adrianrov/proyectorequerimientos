﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication.Controllers
{
    public class NewsController : Controller
    {
        //
        // GET: /News/

        [HttpGet]
        public PartialViewResult Create()
        {
            return PartialView(new WebApplication.Models.NewsModels());
        }

        [HttpPost]
        public JsonResult Create(WebApplication.noticias noticia)
        {
            hrdbEntities db = new hrdbEntities();
            db.noticias.Add(noticia);
            db.SaveChanges();
            return Json(noticia, JsonRequestBehavior.AllowGet);
        }

    }
}
