﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication
{
    public partial class noticias
    {
        public static IQueryable<noticias> Select(hrdbEntities db)
        {
            var query = from noticia in db.noticias
                        select noticia;
            return query;
        }

        public bool create(hrdbEntities db)
        {
            noticias not = new noticias();
            not.Contenido = "holi";
            not.Fecha = DateTime.Now;
            not.Titulo = "noticia de prueba";
            db.noticias.Add(not);
            return true;
        }

    }
}