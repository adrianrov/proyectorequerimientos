CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `vista_noticias` AS
    select 
        `t1`.`idNoticia` AS `ID_Noticia`,
        `t1`.`Titulo` AS `Titulo`,
        `t1`.`Contenido` AS `Contenido`,
        `t1`.`Fecha` AS `Fecha`,
        `t2`.`idImagen` AS `ID_Imagen`
    from
        (`noticias` `t1`
        join `imagenesdenoticia` `t2`)
    where
        (`t2`.`idNoticia` = `t1`.`idNoticia`)