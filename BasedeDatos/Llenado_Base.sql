/*SCRIPT LLENADO BASE DE DATOS HOTELES DEL RECUERDO*/

/******************Insertar Paises*******************/
INSERT INTO paises (Nombre) values ('Costa Rica');
INSERT INTO paises (Nombre) values ('Estados Unidos');
INSERT INTO paises (Nombre) values ('Panama');

/*******************Insertar sedes*******************/
INSERT INTO sedes (Nombre) values ('San Carlos');
INSERT INTO sedes (Nombre) values ('Jaco');
INSERT INTO sedes (Nombre) values ('Miami');
INSERT INTO sedes (Nombre) values ('Ciudad de Panama');

/*******************Insertar sedes de paises*******************/
INSERT INTO sedesdepais (idPais,idSede) values ((SELECT idPais from paises where Nombre='Costa Rica'),
												(SELECT idSede from sedes where Nombre='San Carlos'));
INSERT INTO sedesdepais (idPais,idSede) values ((SELECT idPais from paises where Nombre='Costa Rica'),
												(SELECT idSede from sedes where Nombre='Jaco'));
INSERT INTO sedesdepais (idPais,idSede) values ((SELECT idPais from paises where Nombre='Estados Unidos'),
												(SELECT idSede from sedes where Nombre='Miami'));
INSERT INTO sedesdepais (idPais,idSede) values ((SELECT idPais from paises where Nombre='Panama'),
												(SELECT idSede from sedes where Nombre='Ciudad de Panama'));

/*******************Insertar tipos de habitacion*******************/
INSERT INTO tiposdehabitacion (Nombre,Precio) values ('Sencilla',100);
INSERT INTO tiposdehabitacion (Nombre,Precio) values ('Doble',150);
INSERT INTO tiposdehabitacion (Nombre,Precio) values ('Familiar',170);
INSERT INTO tiposdehabitacion (Nombre,Precio) values ('Big Family',200);
INSERT INTO tiposdehabitacion (Nombre,Precio) values ('Suite',600);

/*******************Insertar servicios*******************/
INSERT INTO servicios (Nombre) values ('Alimentacion');
INSERT INTO servicios (Nombre) values ('Piscina');
INSERT INTO servicios (Nombre) values ('Traslado Hotel-Aeropuerto-Hotel');
INSERT INTO servicios (Nombre) values ('Gimnasio');
INSERT INTO servicios (Nombre) values ('Servicio a la habitacion');
INSERT INTO servicios (Nombre) values ('Clases de surf');
INSERT INTO servicios (Nombre) values ('Clases de baile');
INSERT INTO servicios (Nombre) values ('Show nocturno');
INSERT INTO servicios (Nombre) values ('Discoteca');

/*******************Insertar servicios de sedes*******************/
/*San Carlos*/
INSERT INTO serviciosdesede (idSede,idServicio) values ((SELECT idSede from sedes where Nombre='San Carlos'),
												(SELECT idServicio from servicios where Nombre='Alimentacion'));
INSERT INTO serviciosdesede (idSede,idServicio) values ((SELECT idSede from sedes where Nombre='San Carlos'),
												(SELECT idServicio from servicios where Nombre='Piscina'));
INSERT INTO serviciosdesede (idSede,idServicio) values ((SELECT idSede from sedes where Nombre='San Carlos'),
												(SELECT idServicio from servicios where Nombre='Show nocturno'));
INSERT INTO serviciosdesede (idSede,idServicio) values ((SELECT idSede from sedes where Nombre='San Carlos'),
												(SELECT idServicio from servicios where Nombre='Gimnasio'));
/*Jaco*/
INSERT INTO serviciosdesede (idSede,idServicio) values ((SELECT idSede from sedes where Nombre='Jaco'),
												(SELECT idServicio from servicios where Nombre='Alimentacion'));
INSERT INTO serviciosdesede (idSede,idServicio) values ((SELECT idSede from sedes where Nombre='Jaco'),
												(SELECT idServicio from servicios where Nombre='Piscina'));
INSERT INTO serviciosdesede (idSede,idServicio) values ((SELECT idSede from sedes where Nombre='Jaco'),
												(SELECT idServicio from servicios where Nombre='Show nocturno'));
INSERT INTO serviciosdesede (idSede,idServicio) values ((SELECT idSede from sedes where Nombre='Jaco'),
												(SELECT idServicio from servicios where Nombre='Servicio a la habitacion'));
INSERT INTO serviciosdesede (idSede,idServicio) values ((SELECT idSede from sedes where Nombre='Jaco'),
												(SELECT idServicio from servicios where Nombre='Clases de surf'));
INSERT INTO serviciosdesede (idSede,idServicio) values ((SELECT idSede from sedes where Nombre='Jaco'),
												(SELECT idServicio from servicios where Nombre='Traslado Hotel-Aeropuerto-Hotel'));
/*Miami*/
INSERT INTO serviciosdesede (idSede,idServicio) values ((SELECT idSede from sedes where Nombre='Miami'),
												(SELECT idServicio from servicios where Nombre='Alimentacion'));
INSERT INTO serviciosdesede (idSede,idServicio) values ((SELECT idSede from sedes where Nombre='Miami'),
												(SELECT idServicio from servicios where Nombre='Piscina'));
INSERT INTO serviciosdesede (idSede,idServicio) values ((SELECT idSede from sedes where Nombre='Miami'),
												(SELECT idServicio from servicios where Nombre='Show nocturno'));
INSERT INTO serviciosdesede (idSede,idServicio) values ((SELECT idSede from sedes where Nombre='Miami'),
												(SELECT idServicio from servicios where Nombre='Servicio a la habitacion'));
INSERT INTO serviciosdesede (idSede,idServicio) values ((SELECT idSede from sedes where Nombre='Miami'),
												(SELECT idServicio from servicios where Nombre='Clases de surf'));
INSERT INTO serviciosdesede (idSede,idServicio) values ((SELECT idSede from sedes where Nombre='Miami'),
												(SELECT idServicio from servicios where Nombre='Clases de baile'));
INSERT INTO serviciosdesede (idSede,idServicio) values ((SELECT idSede from sedes where Nombre='Miami'),
												(SELECT idServicio from servicios where Nombre='Traslado Hotel-Aeropuerto-Hotel'));
/*Ciudad de Panama*/
INSERT INTO serviciosdesede (idSede,idServicio) values ((SELECT idSede from sedes where Nombre='Ciudad de Panama'),
												(SELECT idServicio from servicios where Nombre='Alimentacion'));
INSERT INTO serviciosdesede (idSede,idServicio) values ((SELECT idSede from sedes where Nombre='Ciudad de Panama'),
												(SELECT idServicio from servicios where Nombre='Piscina'));
INSERT INTO serviciosdesede (idSede,idServicio) values ((SELECT idSede from sedes where Nombre='Ciudad de Panama'),
												(SELECT idServicio from servicios where Nombre='Show nocturno'));
INSERT INTO serviciosdesede (idSede,idServicio) values ((SELECT idSede from sedes where Nombre='Ciudad de Panama'),
												(SELECT idServicio from servicios where Nombre='Servicio a la habitacion'));
INSERT INTO serviciosdesede (idSede,idServicio) values ((SELECT idSede from sedes where Nombre='Ciudad de Panama'),
												(SELECT idServicio from servicios where Nombre='Clases de baile'));
INSERT INTO serviciosdesede (idSede,idServicio) values ((SELECT idSede from sedes where Nombre='Ciudad de Panama'),
												(SELECT idServicio from servicios where Nombre='Traslado Hotel-Aeropuerto-Hotel'));

/*******************User Profile*******************/
INSERT INTO userprofile (UserName) values ('arodriguez');
INSERT INTO userprofile (UserName) values ('jcastro');
INSERT INTO userprofile (UserName) values ('mcalderon');

/*******************Tarjetas*******************/
INSERT INTO tarjetas (Nombre,Mes,Ano,Codigo,idUsuario) values ('Adrian Rodriguez',04,2015,271,
														(SELECT UserId from userprofile where UserName='arodriguez'));
INSERT INTO tarjetas (Nombre,Mes,Ano,Codigo,idUsuario) values ('Julio Castro',06,2016,471,
														(SELECT UserId from userprofile where UserName='jcastro'));
INSERT INTO tarjetas (Nombre,Mes,Ano,Codigo,idUsuario) values ('Marcos Calderon',09,2017,671,
														(SELECT UserId from userprofile where UserName='mcalderon'));

/*******************Habitaciones*******************/
/*Sencillas*/
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (01,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Sencilla'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (02,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Sencilla'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (03,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Sencilla'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (04,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Sencilla'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (05,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Sencilla'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (06,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Sencilla'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (07,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Sencilla'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (08,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Sencilla'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (09,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Sencilla'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (10,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Sencilla'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (11,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Sencilla'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (12,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Sencilla'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (13,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Sencilla'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (14,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Sencilla'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (15,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Sencilla'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (16,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Sencilla'));
/*Dobles*/
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (201,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Doble'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (202,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Doble'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (203,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Doble'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (204,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Doble'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (205,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Doble'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (206,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Doble'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (207,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Doble'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (208,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Doble'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (209,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Doble'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (210,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Doble'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (211,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Doble'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (212,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Doble'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (213,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Doble'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (214,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Doble'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (215,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Doble'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (216,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Doble'));
/*Suites*/
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (401,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Suite'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (402,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Suite'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (403,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Suite'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (404,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Suite'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (405,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Suite'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (406,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Suite'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (407,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Suite'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (408,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Suite'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (409,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Suite'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (410,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Suite'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (411,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Suite'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (412,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Suite'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (413,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Suite'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (414,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Suite'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (415,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Suite'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (416,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Suite'));
/*Familiar*/
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (601,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Familiar'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (602,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Familiar'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (603,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Familiar'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (604,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Familiar'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (605,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Familiar'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (606,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Familiar'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (607,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Familiar'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (608,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Familiar'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (609,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Familiar'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (610,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Familiar'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (611,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Familiar'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (612,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Familiar'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (613,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Familiar'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (614,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Familiar'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (615,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Familiar'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (616,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Familiar'));
/*Big Family*/
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (801,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Big Family'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (802,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Big Family'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (803,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Big Family'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (804,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Big Family'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (805,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Big Family'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (806,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Big Family'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (807,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Big Family'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (808,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Big Family'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (809,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Big Family'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (810,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Big Family'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (811,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Big Family'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (812,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Big Family'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (813,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Big Family'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (814,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Big Family'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (815,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Big Family'));
INSERT INTO habitaciones (Numero,TiposdeHabitacion_idTipodeHabitacion) values (816,
														(SELECT idTipodeHabitacion from tiposdehabitacion where Nombre='Big Family'));

/*******************Habitaciones de sedes*******************/

/*San Carlos*/
			/*//Sencillas//*/
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 01),
															(Select idSede from sedes where Nombre = 'San Carlos'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 02),
															(Select idSede from sedes where Nombre = 'San Carlos'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 03),
															(Select idSede from sedes where Nombre = 'San Carlos'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 04),
															(Select idSede from sedes where Nombre = 'San Carlos'));
/*//Dobles//*/
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 201),
															(Select idSede from sedes where Nombre = 'San Carlos'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 202),
															(Select idSede from sedes where Nombre = 'San Carlos'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 203),
															(Select idSede from sedes where Nombre = 'San Carlos'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 204),
															(Select idSede from sedes where Nombre = 'San Carlos'));
/*//Suites//*/
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 401),
															(Select idSede from sedes where Nombre = 'San Carlos'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 402),
															(Select idSede from sedes where Nombre = 'San Carlos'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 403),
															(Select idSede from sedes where Nombre = 'San Carlos'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 404),
															(Select idSede from sedes where Nombre = 'San Carlos'));
/*//Familiar//*/
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 601),
															(Select idSede from sedes where Nombre = 'San Carlos'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 602),
															(Select idSede from sedes where Nombre = 'San Carlos'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 603),
															(Select idSede from sedes where Nombre = 'San Carlos'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 604),
															(Select idSede from sedes where Nombre = 'San Carlos'));
/*//Big Family//*/
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 801),
															(Select idSede from sedes where Nombre = 'San Carlos'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 802),
															(Select idSede from sedes where Nombre = 'San Carlos'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 803),
															(Select idSede from sedes where Nombre = 'San Carlos'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 804),
															(Select idSede from sedes where Nombre = 'San Carlos'));

/*Jaco*/
				/*//Sencillas//*/
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 05),
															(Select idSede from sedes where Nombre = 'Jaco'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 06),
															(Select idSede from sedes where Nombre = 'Jaco'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 07),
															(Select idSede from sedes where Nombre = 'Jaco'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 08),
															(Select idSede from sedes where Nombre = 'Jaco'));
/*//Dobles//*/
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 205),
															(Select idSede from sedes where Nombre = 'Jaco'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 206),
															(Select idSede from sedes where Nombre = 'Jaco'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 207),
															(Select idSede from sedes where Nombre = 'Jaco'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 208),
															(Select idSede from sedes where Nombre = 'Jaco'));
/*//Suite//*/
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 405),
															(Select idSede from sedes where Nombre = 'Jaco'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 406),
															(Select idSede from sedes where Nombre = 'Jaco'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 407),
															(Select idSede from sedes where Nombre = 'Jaco'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 408),
															(Select idSede from sedes where Nombre = 'Jaco'));
/*//Familiar//*/
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 605),
															(Select idSede from sedes where Nombre = 'Jaco'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 606),
															(Select idSede from sedes where Nombre = 'Jaco'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 607),
															(Select idSede from sedes where Nombre = 'Jaco'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 608),
															(Select idSede from sedes where Nombre = 'Jaco'));
/*//Big Family//*/
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 805),
															(Select idSede from sedes where Nombre = 'Jaco'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 806),
															(Select idSede from sedes where Nombre = 'Jaco'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 807),
															(Select idSede from sedes where Nombre = 'Jaco'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 808),
															(Select idSede from sedes where Nombre = 'Jaco'));

/*Miami*/
		/*//Sencillas//*/
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 09),
															(Select idSede from sedes where Nombre = 'Miami'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 10),
															(Select idSede from sedes where Nombre = 'Miami'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 11),
															(Select idSede from sedes where Nombre = 'Miami'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 12),
															(Select idSede from sedes where Nombre = 'Miami'));
/*//Dobles//*/
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 209),
															(Select idSede from sedes where Nombre = 'Miami'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 210),
															(Select idSede from sedes where Nombre = 'Miami'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 211),
															(Select idSede from sedes where Nombre = 'Miami'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 212),
															(Select idSede from sedes where Nombre = 'Miami'));

/*//Suites//*/
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 409),
															(Select idSede from sedes where Nombre = 'Miami'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 410),
															(Select idSede from sedes where Nombre = 'Miami'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 411),
															(Select idSede from sedes where Nombre = 'Miami'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 412),
															(Select idSede from sedes where Nombre = 'Miami'));

/*//Familiar//*/
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 609),
															(Select idSede from sedes where Nombre = 'Miami'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 610),
															(Select idSede from sedes where Nombre = 'Miami'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 611),
															(Select idSede from sedes where Nombre = 'Miami'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 612),
															(Select idSede from sedes where Nombre = 'Miami'));

/*//Big Family//*/
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 809),
															(Select idSede from sedes where Nombre = 'Miami'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 810),
															(Select idSede from sedes where Nombre = 'Miami'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 811),
															(Select idSede from sedes where Nombre = 'Miami'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 812),
															(Select idSede from sedes where Nombre = 'Miami'));

/*Panama*/
		/*//Sencillas//*/
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 13),
															(Select idSede from sedes where Nombre = 'Ciudad de Panama'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 14),
															(Select idSede from sedes where Nombre = 'Ciudad de Panama'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 15),
															(Select idSede from sedes where Nombre = 'Ciudad de Panama'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 16),
															(Select idSede from sedes where Nombre = 'Ciudad de Panama'));
		/*//Dobles//*/
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 213),
															(Select idSede from sedes where Nombre = 'Ciudad de Panama'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 214),
															(Select idSede from sedes where Nombre = 'Ciudad de Panama'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 215),
															(Select idSede from sedes where Nombre = 'Ciudad de Panama'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 216),
															(Select idSede from sedes where Nombre = 'Ciudad de Panama'));
		/*//Suites//*/
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 413),
															(Select idSede from sedes where Nombre = 'Ciudad de Panama'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 414),
															(Select idSede from sedes where Nombre = 'Ciudad de Panama'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 415),
															(Select idSede from sedes where Nombre = 'Ciudad de Panama'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 416),
															(Select idSede from sedes where Nombre = 'Ciudad de Panama'));
		/*//Familiar//*/
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 613),
															(Select idSede from sedes where Nombre = 'Ciudad de Panama'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 614),
															(Select idSede from sedes where Nombre = 'Ciudad de Panama'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 615),
															(Select idSede from sedes where Nombre = 'Ciudad de Panama'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 616),
															(Select idSede from sedes where Nombre = 'Ciudad de Panama'));
		/*//Big Family//*/
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 813),
															(Select idSede from sedes where Nombre = 'Ciudad de Panama'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 814),
															(Select idSede from sedes where Nombre = 'Ciudad de Panama'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 815),
															(Select idSede from sedes where Nombre = 'Ciudad de Panama'));
INSERT INTO habitacionesdesede (idHabitacion,idSede) values ((Select idHabitacion from habitaciones where Numero = 816),
															(Select idSede from sedes where Nombre = 'Ciudad de Panama'));

/*******************Reservaciones*******************/
INSERT INTO reservaciones (Fecha,idTarjeta,idUsuario) values (NOW(),(Select idTarjeta from tarjetas where Nombre='Adrian Rodriguez'),
																	(Select UserId from userprofile where UserName='arodriguez'));
INSERT INTO reservaciones (Fecha,idTarjeta,idUsuario) values (NOW(),(Select idTarjeta from tarjetas where Nombre='Julio Castro'),
																	(Select UserId from userprofile where UserName='jcastro'));
INSERT INTO reservaciones (Fecha,idTarjeta,idUsuario) values (NOW(),(Select idTarjeta from tarjetas where Nombre='Marcos Calderon'),
																	(Select UserId from userprofile where UserName='mcalderon'));
/*******************Habitaciones de reserva*******************/
INSERT INTO habitacionesdereservacion (idReservacion,idHabitacion) values ((Select idReservacion from reservaciones where idUsuario=
																				(Select UserId from userprofile where UserName='arodriguez')),
																			(Select idHabitacion from habitaciones where Numero=205));
INSERT INTO habitacionesdereservacion (idReservacion,idHabitacion) values ((Select idReservacion from reservaciones where idUsuario=
																				(Select UserId from userprofile where UserName='jcastro')),
																			(Select idHabitacion from habitaciones where Numero=602));
INSERT INTO habitacionesdereservacion (idReservacion,idHabitacion) values ((Select idReservacion from reservaciones where idUsuario=
																				(Select UserId from userprofile where UserName='mcalderon')),
																			(Select idHabitacion from habitaciones where Numero=810));

/*******************Paquetes*******************/
INSERT INTO paquetes (Nombre,Agotado,Habilitado,FechadeInicio,FechadeFin,FechadeExpiracion,Descripcion)
	values ('Fin de Semana en Familia',0,1,NOW(),'2014-05-30 18:15:02','2014-06-07 18:15:02','Una habitacion tipo Familiar para cualquier fin de semana de estos dos meses');

INSERT INTO paquetes (Nombre,Agotado,Habilitado,FechadeInicio,FechadeFin,FechadeExpiracion,Descripcion)
	values ('Entre Dos Es Mejor',0,1,NOW(),'2014-06-30 18:15:02','2014-09-07 18:15:02','Una habitacion tipo Suite con alimentacion incluida y clases de surf para cualquier dia entre semana');
 
/*******************Habitaciones de paquete*******************/
INSERT INTO habitacionesdepaquete (Paquetes_idPaquete,Habitaciones_idHabitacion) values ((Select idPaquete from paquetes where Nombre='Fin de Semana en Familia'),
																			(Select idHabitacion from habitaciones where Numero=604));
INSERT INTO habitacionesdepaquete (Paquetes_idPaquete,Habitaciones_idHabitacion) values ((Select idPaquete from paquetes where Nombre='Fin de Semana en Familia'),
																			(Select idHabitacion from habitaciones where Numero=606));
INSERT INTO habitacionesdepaquete (Paquetes_idPaquete,Habitaciones_idHabitacion) values ((Select idPaquete from paquetes where Nombre='Fin de Semana en Familia'),
																			(Select idHabitacion from habitaciones where Numero=607));
INSERT INTO habitacionesdepaquete (Paquetes_idPaquete,Habitaciones_idHabitacion) values ((Select idPaquete from paquetes where Nombre='Fin de Semana en Familia'),
																			(Select idHabitacion from habitaciones where Numero=613));
INSERT INTO habitacionesdepaquete (Paquetes_idPaquete,Habitaciones_idHabitacion) values ((Select idPaquete from paquetes where Nombre='Entre Dos Es Mejor'),
																			(Select idHabitacion from habitaciones where Numero=405));
INSERT INTO habitacionesdepaquete (Paquetes_idPaquete,Habitaciones_idHabitacion) values ((Select idPaquete from paquetes where Nombre='Entre Dos Es Mejor'),
																			(Select idHabitacion from habitaciones where Numero=406));
INSERT INTO habitacionesdepaquete (Paquetes_idPaquete,Habitaciones_idHabitacion) values ((Select idPaquete from paquetes where Nombre='Entre Dos Es Mejor'),
																			(Select idHabitacion from habitaciones where Numero=412));

/*******************Promociones*******************/
INSERT INTO promociones (Nombre,FechadeInicio,FechadeFin,FechadeExpiracion,Descripcion)
	values ('2x1 entre lunes y miercoles',NOW(),'2014-05-30 18:15:02','2014-06-30 18:15:02','2 noches en una habitacion tipo Doble para cualquier dia entrando lunes o martes por el precio de una');

INSERT INTO promociones (Nombre,FechadeInicio,FechadeFin,FechadeExpiracion,Descripcion)
	values ('Noche gratis en tu cumpleanos',NOW(),'2014-09-30 18:15:02','2014-10-30 18:15:02','Una noche gratis en una habitacion tipo Big Family al presentar tu cedula en el dia de tu cumpleanos');

/*******************Habitaciones de promocion*******************/
INSERT INTO habitacionesdepromocion (idPromocion,idHabitacion) values ((Select idPromocion from promociones where Nombre='2x1 entre lunes y miercoles'),
																			(Select idHabitacion from habitaciones where Numero=212));
INSERT INTO habitacionesdepromocion (idPromocion,idHabitacion) values ((Select idPromocion from promociones where Nombre='2x1 entre lunes y miercoles'),
																			(Select idHabitacion from habitaciones where Numero=201));
INSERT INTO habitacionesdepromocion (idPromocion,idHabitacion) values ((Select idPromocion from promociones where Nombre='2x1 entre lunes y miercoles'),
																			(Select idHabitacion from habitaciones where Numero=207));
INSERT INTO habitacionesdepromocion (idPromocion,idHabitacion) values ((Select idPromocion from promociones where Nombre='2x1 entre lunes y miercoles'),
																			(Select idHabitacion from habitaciones where Numero=216));
INSERT INTO habitacionesdepromocion (idPromocion,idHabitacion) values ((Select idPromocion from promociones where Nombre='Noche gratis en tu cumpleanos'),
																			(Select idHabitacion from habitaciones where Numero=816));
INSERT INTO habitacionesdepromocion (idPromocion,idHabitacion) values ((Select idPromocion from promociones where Nombre='Noche gratis en tu cumpleanos'),
																			(Select idHabitacion from habitaciones where Numero=802));
INSERT INTO habitacionesdepromocion (idPromocion,idHabitacion) values ((Select idPromocion from promociones where Nombre='Noche gratis en tu cumpleanos'),
																			(Select idHabitacion from habitaciones where Numero=808));

/*******************Noticias*******************/
INSERT INTO noticias (Titulo,Contenido,Fecha) values ('Viaje a Panama','Disfruta de un viaje todo incluido a la mistica ciudad de Panama con todo incluido este inicio de año. Pronto mas informacion',NOW());
INSERT INTO noticias (Titulo,Contenido,Fecha) values ('Tour Cavernas','Disfruta de un viaje inolvidable a las cavernas en un tour de un dia durante la epoca de vacaciones. Pronto mas informacion',NOW());
INSERT INTO noticias (Titulo,Contenido,Fecha) values ('Premio al mejor hotel','Recientemente nuestra cadena Hoteles del Recuerdo ha ganado el premio a mejor hotel de la zona en el concurso The Best Hotel CR efectuado el pasado 10 de abril',NOW());


/*******************Imagenes*******************/
/*******************Imagenes de noticia*******************/
/*******************Imagenes de promocion*******************/
/*******************Galerias*******************/
