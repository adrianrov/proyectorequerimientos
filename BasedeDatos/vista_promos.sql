CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `vista_promos` AS
    select 
        `t1`.`idPromocion` AS `ID_Promo`,
        `t1`.`Nombre` AS `Nombre`,
        `t1`.`FechadeInicio` AS `Inicio`,
        `t1`.`FechadeFin` AS `Fin`,
        `t1`.`FechadeExpiracion` AS `Expira`,
        `t1`.`Descripcion` AS `Descripcion`,
        `t4`.`Numero` AS `#_HAB`,
        `t5`.`Nombre` AS `Tipo_HAB`,
		`t6`.`URL` AS 'URL_Imagen'
    from
        (((((`promociones` `t1`
        join `habitacionesdepromocion` `t3`)
        join `habitaciones` `t4`)
        join `tiposdehabitacion` `t5`)
		join `imagenes` `t6`)
		join `imagenesdepromocion` `t7`)
    where
        ((`t3`.`idPromocion` = `t1`.`idPromocion`)
            and (`t3`.`idHabitacion` = `t4`.`idHabitacion`)
            and (`t4`.`TiposdeHabitacion_idTipodeHabitacion` = `t5`.`idTipodeHabitacion`)
			and (`t6`.`idImagen` = `t7`.`idImagen`)
			and (`t1`.`idPromocion` = `t7`.`idPromocion`))